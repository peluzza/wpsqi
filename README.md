# This is my README

Hola chicos, esta es la primera release, llena de fallos bug (me gusta llamarlo features... ;) )

Por ahora el script tiene las siguientes dependencias:

- python - de serie en casi todas las distros (menos openwrt... ¬ ¬ )
- easy_install _
- aircrack-ng
- wash (por ahora no tengo substituto propio, pero todo se andará)
- bully

Instalación:

Si utilizais wifislax ya teneis todo lo que hace falta. Solo se necesita ya instalar un módulo externo de python, pero estoy trabajando para incluirlo de serie en el package.

es tan sencillo como ejecutar "easy_instal netifaces"

y ya está, solo queda ejecutar el archivo wps-qi.py con privilegios de root.

para ejecutarlo podeis hacerlo ejecutable "chmod +X wps-qi.py" y ./wps-qi.py

o bien ejecutarlo a través del interprete de python sudo python wps-qi.py


Sobre el output de bully

Hasta ahora el código está muy limpio hasta que llega el momento de ejecutar a bully. me ha dado muchos dolores de cabeza un
pequeño con la captura del stdout, por lo que llega un poco tarde y todavía no esta lo depurado que quisiera.
Es mi tarea para estos días.

Os pido que analiceis en profundidad el rendimiento de bully, si os parece que va lento, rápido o como os parezca que tira, ya
que hace falta un poco de tunning para afinarlo. En el futuro, se adaptará el comando para lanzar bully en función del router que 
tengamos como objetivo.

En ese aspecto ando un poco perdido, pero tengo la sensación de que bully "aturulla" los router un poco pronto.
Si vais a la linea 142 del código vereis esto:

    def bully(iface, target, tmp_folder="./", custom="-F -N -1 1,1 -l 60"):
	
En esta linea, el campo custom es donde he metido mis parámetros personalizados para bully, a fin de conseguir un ataque lo más 
estable posible. Es decir, la línea que ejecuta bully es:

    bully mon0 -e ESSID -c X -b aa:bb:cc:dd:ee:ff -w ./tmp [ todos los parametros dentro de custom ] -v 3 
	
Os pido que si teneis sugerencias tanto en general como en algun router en particular, me lo hagais llegar.


Aun hay mucho trabajo por delante, pero según el ritmo que llevo, calculo que para navidades ya tenga un herramienta totalmente
funcional con un par de trucos nuevos: detectar cuando el objetivo se "queda tostado" 5 minutos, y aprovechar ese tiempo para 
auditar otro router que se habrá añadido como objetivo secundario.

Vuestro feedback será muy muy bien recibido, os pido que me lo hagais llegar a traves de MP del foro.
Un saludo.