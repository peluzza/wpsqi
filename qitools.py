#!/usr/bin/env python
# -*- coding: utf-8 -*-

# COMPROBAR SISTEMA  ##


def check_root():
    import os
    if os.geteuid() != 0:
        print "[!] - Error: You need root privileges to run this script"
        exit(
            "[i] - Use 'sudo' or login with root user ")


# crea un archivo con el contenido que se indica
def make_file(filename, data=""):
    #import re
    f = open(filename, 'w')
    f.write(data)
    f.close


# comprueba si existe un archivo, y si se indica, lo crea
def check_file(filename, create=False, data=""):
    import os.path
    if not os.path.isfile(filename):
        if create is True:
            make_file(filename, str(data))
        else:
            print ("[!] - File %s not found" % filename)
    else:
        return True


# comprueba si existe una carpeta, y si se indica, la crea
def check_folder(folder, create):
    import os.path
    if not os.path.isdir(folder):
        if create is True:
            os.makedirs(folder)
        else:
            print ("[!] - Folder %s not found" % folder)


def clean_tmp_scan():
    # borrar temporales
    import os
    # <= esto debe ir en variable de script washscan(iface, temps)
    temps = "./wps/tmp/redes_wps.lst"
    if check_file(temps, "", False) is True:
        try:
            os.popen("rm -rf ./wps/tmp/redes_wps.lst")
            print('previous session deleted')
        except:
            print "error while cleaning previous sessions"
    else:
        print "cache is empty (and it's good')"


def ctrlc(signal, frame):
    from candy import fail
    import sys
    sys.stdout.write("\n")
    fail("Session closed by user. System exit.")
    sys.exit(0)


# INTERFACES DE RED #


# devuelve una -lista- de interfaces dado un nombre
def get_ifaces_odd(iface_name):
    try:
        import netifaces
        ifaces = netifaces.interfaces()
    except:
        "netifaces module required. run easy_install netifaces to get it."
    l = []
    for line in ifaces:
        if line.startswith(iface_name):
            l.append(line)
    return l


# obtiene el driver que utiliza el dispositivo dado
def get_driver_odd(iface):
    import subprocess
    command = str("ls /sys/class/net/%s/device/driver/module/drivers/" % iface)
    print command
    p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    return p.stdout


# levanta o baja la interfaz seleccionada (solo mon)
def manage_mon_iface(iface, ssignal):  # ssignal =>  start / stop
    import subprocess
    try:
        command = str('airmon-ng %s %s' % (ssignal, iface))
        subprocess.call(command, shell=True, stdout=subprocess.PIPE)
    except:
        print "[!] - Error: while executing airmon-ng'"


# mata todas las interfaces mon
def down_all_mon():
    import time
    import candy
    candy.info("Killing previous monitor interfaces")
    monifaces = get_ifaces_odd("mon")
    if monifaces == []:
        return
    else:
        for mon in monifaces:
            candy.pad("killing %s" % mon)
            manage_mon_iface(mon, "stop")
            time.sleep(1)
        return

#
# MANEJO DE PROCESOS ###
#


# busca procesos del comando de sistema dado
def get_pid(name):
    import subprocess
    command = str(("""pgrep %s""") % name)
    p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    procs = []
    salida = p.stdout
    for line in salida:
        procs.append(str.strip(line))

    return procs


# mata todos los procesos del comando de sistema dado
def kill_pid(name):
    import os
    import signal
    import candy
    if get_pid(name) == []:
        pass
    else:
        for PID in get_pid(name):
            try:
                candy.pad("killing %s processes." % name)
                os.kill(int(PID), signal.SIGTERM)
            except OSError as ex:
                raise Exception(candy.fail(
                    "[!] - Unable to kill %s %s" % (name, PID)))
                return ex
                continue


def kill_procs():
    import candy
    candy.info("killing problematic processes")
    Procs = [
        'wicd',
        'wash',
        'reaver',
        'bully',
        'avahi-daemon',
        'avahi-autoipd',
        'ifconfig',
        'dhcpcd',
        'dhclient',
        'dhclient3',
        'NetworkManager',
        'wpa_supplicant',
        'udhcpc']
    for proc in Procs:
        kill_pid(proc)
